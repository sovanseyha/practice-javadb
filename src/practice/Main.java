package practice;
import javax.xml.transform.Result;
import java.sql.*;
import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);

    static Employee insertEmp(){
        System.out.println("Enter Employee Name    : ");
        String name = scanner.next();
        System.out.println("Enter Employee Address : ");
        String address = scanner.next();
        return new Employee(name,address);
    }

    static void selectQuery(Connection connection){
        String selectQeury = "select * from seyha_tb";
        try {
            Statement statement = connection.createStatement();
            ResultSet res = statement.executeQuery(selectQeury);

            while (res.next()){
                System.out.println(res.getString("name"));
                System.out.println(res.getString("address"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    static void insertQuery(Connection connection){
        String insertQuery = "insert into seyha_tb values(default,?,?)";
        Employee emp = insertEmp();

        try{
            PreparedStatement statement = connection.prepareStatement(insertQuery);
            statement.setString(1, emp.getName());
            statement.setString(2,emp.getAddress());
            int row = statement.executeUpdate();
            if(row>0){
                System.out.println("Insert Employee Success !!");
            }else{
                System.out.println("Fail To Insert Employee !!");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    static void updateEmployee(Connection connection){
        System.out.println("Enter Employee ID :");
        int id = scanner.nextInt();
        String updateQuery = "update seyha_tb1 set name=?,address=? where id=?";
        Employee emp = insertEmp();
        try{
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            statement.setString(1, emp.getName());
            statement.setString(2, emp.getAddress());
            statement.setInt(3,id);
            int row = statement.executeUpdate();
            if(row>0){
                System.out.println("Update Successfully !!");
            }else{
                System.out.println("Fail To Update !! ");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        MyConnection connectDB = new MyConnection();
        Connection connection = connectDB.getConnection();
        int op = 0;

        selectQuery(connection);
        do{
            System.out.println("1-(Insert) || 2-(Update)");
            op = scanner.nextInt();
            switch (op){
                case 1:
                    insertQuery(connection);
                    break;
                case 2:
                    updateEmployee(connection);
                    break;
            }
        }while (op<3);
    }
}