package practice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
    Connection getConnection(){
        Connection connection = null;
        try{
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:2003/postgres","postgres","2003");
        }catch(SQLException e){
            e.printStackTrace();
        }
        return connection;
    }
}
